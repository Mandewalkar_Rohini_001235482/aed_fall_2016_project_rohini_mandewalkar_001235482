/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.administrativeRole;

import business.employee.Employee;
import business.enterprise.Enterprise;
import business.organization.Organization;
import business.role.Role;
import business.useraccount.UserAccount;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Vineeth Kashyap
 */
public class ManageUserAccountJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Enterprise enterprise;

    /**
     * Creates new form ManageUserAccountJPanel
     */
    public ManageUserAccountJPanel(JPanel userProcessContainer, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        populateUserAccountJTable();
        populateOrganizationJComboBox();
    }

    public void populateUserAccountJTable() {
        DefaultTableModel dtm = (DefaultTableModel) userAccountsJTable.getModel();
        dtm.setRowCount(0);
        Object[] row = new Object[3];
<<<<<<< HEAD:Mandewalkar_Rohini_001235482-aed_fall_2016_project_rohini_mandewalkar_001235482-8b3369a9cae2/Final_Project/src/userinterface/administrativeRole/ManageUserAccountJPanel.java
        if(enterprise.getEnterpriseType().equals(Enterprise.EnterpriseType.Government))
        {
            
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationDirectory()) {
            for (UserAccount userAccount : organization.getUserAccountDirectory().getUserAccountDirectory()) {
                row[0] = userAccount.getUserID();
                row[1] = userAccount.getUserName();
                row[2] = userAccount.getRole();
               // row[3]  =areaNamejTextField.getText();
                dtm.addRow(row);
            }
        }
        }
        else
        {
            for (Organization organization : enterprise.getOrganizationDirectory().getOrgDirectory()) {
            for (UserAccount userAccount : organization.getUserAccountDirectory().getUserAccountDirectory()) {
                row[0] = userAccount.getUserID();
                row[1] = userAccount.getUserName();
                row[2] = userAccount.getRole();
//               row[3]  =areaNamejTextField.getText();
                dtm.addRow(row);
            }
        }
        }
=======

        if (enterprise.getEnterpriseType().equals(Enterprise.EnterpriseType.Government)) {
            for (Organization organization : enterprise.getOrganizationDirectory().getGovernmentOrganizationDirectory()) {
                for (UserAccount userAccount : organization.getUserAccountDirectory().getUserAccountDirectory()) {
                    row[0] = userAccount.getUserID();
                    row[1] = userAccount.getUserName();
                    row[2] = userAccount.getRole();
                    dtm.addRow(row);
                }
            }
        } else {
            for (Organization organization : enterprise.getOrganizationDirectory().getNgoOrganizationDirectory()) {
                for (UserAccount userAccount : organization.getUserAccountDirectory().getUserAccountDirectory()) {
                    row[0] = userAccount.getUserID();
                    row[1] = userAccount.getUserName();
                    row[2] = userAccount.getRole();
                    dtm.addRow(row);
                }
            }
        }

>>>>>>> 4b9628a385eae208904823e559b13682d3c37411:Final_Project/src/userinterface/administrativeRole/ManageUserAccountJPanel.java
    }

    public void populateOrganizationJComboBox() {
        organizationJComboBox.removeAllItems();
<<<<<<< HEAD:Mandewalkar_Rohini_001235482-aed_fall_2016_project_rohini_mandewalkar_001235482-8b3369a9cae2/Final_Project/src/userinterface/administrativeRole/ManageUserAccountJPanel.java
        if(enterprise.getEnterpriseType().equals(Enterprise.EnterpriseType.Government))
        {
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationDirectory()) {
            organizationJComboBox.addItem(organization);
        }   
        }
        else
        {
           for (Organization organization : enterprise.getOrganizationDirectory().getOrgDirectory()) {
            organizationJComboBox.addItem(organization);
        }  
        }
=======

        if (enterprise.getEnterpriseType().equals(Enterprise.EnterpriseType.Government)) {
            for (Organization organization : enterprise.getOrganizationDirectory().getGovernmentOrganizationDirectory()) {
                organizationJComboBox.addItem(organization);
            }
        } else {
            for (Organization organization : enterprise.getOrganizationDirectory().getNgoOrganizationDirectory()) {
                organizationJComboBox.addItem(organization);
            }
        }

>>>>>>> 4b9628a385eae208904823e559b13682d3c37411:Final_Project/src/userinterface/administrativeRole/ManageUserAccountJPanel.java
    }

    public void populateEmployeeJComboBox(Organization organization) {
        employeeJComboBox.removeAllItems();

        for (Employee employee : organization.getEmployeeDirectory().getEmployeeDirectory()) {
            employeeJComboBox.addItem(employee);
        }
    }

    public void populateRoleJComboBox(Organization organization) {
        roleJComboBox.removeAllItems();

        for (Role role : organization.getSupportedRole()) {
            roleJComboBox.addItem(role);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        backJButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        userAccountsJTable = new javax.swing.JTable();
        createANewUserJLabel = new javax.swing.JLabel();
        organizationJLabel = new javax.swing.JLabel();
        organizationJComboBox = new javax.swing.JComboBox();
        userNameJTextField = new javax.swing.JTextField();
        roleJLabel = new javax.swing.JLabel();
        userNameJLabel = new javax.swing.JLabel();
        roleJComboBox = new javax.swing.JComboBox();
        passwordJLabel = new javax.swing.JLabel();
        passwordJTextField = new javax.swing.JTextField();
        employeeJLabel = new javax.swing.JLabel();
        employeeJComboBox = new javax.swing.JComboBox();
        submitJButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        areaNamejTextField = new javax.swing.JTextField();

        backJButton.setText("<< HEAD BACK TO PREVIOUS PAGE");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        userAccountsJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "USER ID", "USER NAME", "ROLE", "Area"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(userAccountsJTable);

        createANewUserJLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        createANewUserJLabel.setText("CREATE A NEW USER:");

        organizationJLabel.setText("Organization");

        organizationJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                organizationJComboBoxActionPerformed(evt);
            }
        });

        roleJLabel.setText("Role");

        userNameJLabel.setText("User Name");

        passwordJLabel.setText("Password");

        employeeJLabel.setText("Employee");

        submitJButton.setText("SUBMIT");
        submitJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitJButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Area Name");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(submitJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(backJButton)
                            .addComponent(createANewUserJLabel)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(roleJLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(organizationJLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(userNameJLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(passwordJLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(jLabel1))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(userNameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(organizationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(27, 27, 27)
                                        .addComponent(employeeJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(employeeJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(roleJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(areaNamejTextField, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(passwordJTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)))))
                        .addGap(0, 489, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backJButton)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(createANewUserJLabel)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(organizationJLabel)
                    .addComponent(organizationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(employeeJLabel)
                    .addComponent(employeeJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(roleJLabel)
                    .addComponent(roleJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userNameJLabel)
                    .addComponent(userNameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passwordJLabel)
                    .addComponent(passwordJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(areaNamejTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addComponent(submitJButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void organizationJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_organizationJComboBoxActionPerformed
        Organization organization = (Organization) organizationJComboBox.getSelectedItem();
        populateEmployeeJComboBox(organization);
        populateRoleJComboBox(organization);
    }//GEN-LAST:event_organizationJComboBoxActionPerformed

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.remove(this);
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void submitJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitJButtonActionPerformed
        // TODO add your handling code here:
        Organization organization = (Organization) organizationJComboBox.getSelectedItem();
        Employee employee = (Employee) employeeJComboBox.getSelectedItem();
        Role role = (Role) roleJComboBox.getSelectedItem();

        String userName = userNameJTextField.getText();
        String password = passwordJTextField.getText();
<<<<<<< HEAD:Mandewalkar_Rohini_001235482-aed_fall_2016_project_rohini_mandewalkar_001235482-8b3369a9cae2/Final_Project/src/userinterface/administrativeRole/ManageUserAccountJPanel.java
        String area     =areaNamejTextField.getText();
        
        
        
=======

>>>>>>> 4b9628a385eae208904823e559b13682d3c37411:Final_Project/src/userinterface/administrativeRole/ManageUserAccountJPanel.java
        boolean isUnique = organization.getUserAccountDirectory().checkIfUserNameIsUnique(userName);

        if (isUnique) {
            organization.getUserAccountDirectory().addUserAccount(userName, password, role, employee);
            JOptionPane.showMessageDialog(this, "USER ACCOUNT CREATED SUCCESSFULLY", "SUCCESS", JOptionPane.INFORMATION_MESSAGE);
            populateUserAccountJTable();
        } else {
            JOptionPane.showMessageDialog(this, "USERNAME MUST BE UNIQUE...PLEASE TRY A DIFFERENT ONE", "OOPS...", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_submitJButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField areaNamejTextField;
    private javax.swing.JButton backJButton;
    private javax.swing.JLabel createANewUserJLabel;
    private javax.swing.JComboBox employeeJComboBox;
    private javax.swing.JLabel employeeJLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox organizationJComboBox;
    private javax.swing.JLabel organizationJLabel;
    private javax.swing.JLabel passwordJLabel;
    private javax.swing.JTextField passwordJTextField;
    private javax.swing.JComboBox roleJComboBox;
    private javax.swing.JLabel roleJLabel;
    private javax.swing.JButton submitJButton;
    private javax.swing.JTable userAccountsJTable;
    private javax.swing.JLabel userNameJLabel;
    private javax.swing.JTextField userNameJTextField;
    // End of variables declaration//GEN-END:variables
}
