/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue;

import business.useraccount.UserAccount;

/**
 *
 * @author rajas
 */
public class PoliticianWorkRequest extends WorkRequest{
    
    public String status;

    public PoliticianWorkRequest(UserAccount sender, UserAccount receiver, String message, String status) {
        super(sender, receiver, message, status);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
