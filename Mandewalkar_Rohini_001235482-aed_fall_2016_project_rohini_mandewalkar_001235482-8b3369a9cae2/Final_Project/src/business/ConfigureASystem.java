/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import business.geography.City;
import business.geography.Network;
import business.geography.State;
import business.role.SystemAdminRole;
import business.useraccount.UserAccount;

/**
 *
 * @author Vineeth Kashyap
 */
public class ConfigureASystem {
public UserAccount userAccount;
    public static EcoSystem configure() {
        EcoSystem ecoSystem = EcoSystem.getInstance();
        
        Network network=ecoSystem.createNetwork("India");
        
        State state = new State("TG");
        network.getStateList().add(state);
        state.getCityList().add(new City("HYD"));
        
        
        ecoSystem.getUserAccountDirectory().addUserAccount("Admin", "admin", new SystemAdminRole(), ecoSystem.getEmployeeDirectory().createAnEmployee("Vineeth"));
        return ecoSystem;
    }
}
