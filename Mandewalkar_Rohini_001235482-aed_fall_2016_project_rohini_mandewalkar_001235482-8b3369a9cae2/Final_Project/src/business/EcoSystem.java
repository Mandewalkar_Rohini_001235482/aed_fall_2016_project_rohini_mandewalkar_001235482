/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import business.enterprise.Enterprise;
import business.geography.Network;
import business.organization.Organization;
import business.role.Role;
import business.role.SystemAdminRole;
import business.useraccount.UserAccount;
import java.util.ArrayList;

/**
 *
 * @author Vineeth Kashyap
 */
public class EcoSystem extends Organization {

    private static EcoSystem ecoSystem;
    private ArrayList<Network> networkList;

    public static EcoSystem getInstance() {
        if (ecoSystem == null) {
            ecoSystem = new EcoSystem();
        }
        return ecoSystem;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<Network>();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> supportedRoles = new ArrayList<Role>();
        supportedRoles.add(new SystemAdminRole());
        return supportedRoles;
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public Network createNetwork(String name) {
        Network network = new Network(name);
        networkList.add(network);
        return network;
    }

    public boolean checkIfUserNameIsUnique(String userName) {

        for (Network network : networkList) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseDirectory()) {
                if (enterprise.getUserAccountDirectory().checkIfUserNameIsUnique(userName)) {
                    return true;
                }
            }
        }
        return false;
    }

}
