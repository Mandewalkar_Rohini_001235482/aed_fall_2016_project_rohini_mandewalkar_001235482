/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.geography;

import java.util.ArrayList;

/**
 *
 * @author rajas
 */

public class State {
    private String name;
    private ArrayList<City> cityList;
    
    public State(String name)
    {
       this.name=name;
        cityList=new ArrayList<City>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<City> getCityList() {
        return cityList;
    }

    public void setCityList(ArrayList<City> cityList) {
        this.cityList = cityList;
    }
    
}
