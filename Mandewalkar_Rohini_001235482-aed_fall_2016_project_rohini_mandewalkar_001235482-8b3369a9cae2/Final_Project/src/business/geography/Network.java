/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.geography;

import business.enterprise.EnterpriseDirectory;
import java.util.ArrayList;

/**
 *
 * @author Vineeth Kashyap
 */
public class Network {

    private String name;
    private EnterpriseDirectory enterpriseDirectory;
    private int networkID;
    private static int count = 0;
    private ArrayList<State> stateList;

    public Network(String name) {
        this.name = name;
        this.enterpriseDirectory = new EnterpriseDirectory();
        stateList=new ArrayList<State>();
        this.networkID = count + 1;
    }

    public String getName() {
        return name;
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public ArrayList<State> getStateList() {
        return stateList;
    }

    public void setStateList(ArrayList<State> stateList) {
        this.stateList = stateList;
    }
    
    

    @Override
    public String toString() {
        return name;
    }

    public int getNetworkID() {
        return networkID;
    }
}
