/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.role.PoliticianRole;
import business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author Vineeth Kashyap
 */
public class PoliticianOrganization extends Organization{

    public PoliticianOrganization() {
        super(GovernmentOrganizationType.Politician.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> supportedRoles = new ArrayList<Role>();
        supportedRoles.add(new PoliticianRole());
        return supportedRoles;
    }
    
}
