/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.employee.EmployeeDirectory;
import business.role.Role;
import business.useraccount.UserAccountDirectory;
import business.workqueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author Vineeth Kashyap
 */
public abstract class Organization {

    private String name;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private WorkQueue workQueue;
    private int organizationID;
    private static int count;

<<<<<<< HEAD:Mandewalkar_Rohini_001235482-aed_fall_2016_project_rohini_mandewalkar_001235482-8b3369a9cae2/Final_Project/src/business/organization/Organization.java
    public enum OrganizationType {
         Doctor("Doctor Organization"), Politician("Politician Organization"), Finance("Finance Organization"),
=======
    public enum GovernmentOrganizationType {
        Doctor("Doctor Organization"), Politician("Politician Organization"), Finance("Finance Organization"),
>>>>>>> 4b9628a385eae208904823e559b13682d3c37411:Final_Project/src/business/organization/Organization.java
        People("People Organization"), Police("Police Organization"), Teacher("Teacher Organization");
        private String value;

        private GovernmentOrganizationType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
        @Override
        public String toString() {
            return value;
        }
    }
    
    public enum NGOOrganizationType{
        Volunteer("Volunteer Organization");
        private String value;

        private NGOOrganizationType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
        
    }

      /*  @Override
        public String toString() {
            return value;
        }*/
        
        public enum OrgType{
            Volunteer("Volunteer Organization");
            private String value;
            private OrgType(String value)
            {
                this.value=value;
            }

            public String getValue() {
                return value;
            }
            @Override
        public String toString() {
            return value;
        }
        }
         

   
    public Organization(String name) {
        this.name = name;
        this.employeeDirectory = new EmployeeDirectory();
        this.userAccountDirectory = new UserAccountDirectory();
        this.workQueue = new WorkQueue();
        this.organizationID = ++count;
    }

    public String getName() {
        return name;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public int getOrganizationID() {
        return organizationID;
    }
    
    @Override
    public String toString(){
        return name;
    }
    
    public abstract ArrayList<Role> getSupportedRole();

}
