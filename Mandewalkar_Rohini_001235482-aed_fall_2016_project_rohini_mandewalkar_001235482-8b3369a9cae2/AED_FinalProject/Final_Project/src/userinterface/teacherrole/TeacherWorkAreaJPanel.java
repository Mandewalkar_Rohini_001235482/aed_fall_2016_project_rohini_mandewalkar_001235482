/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.teacherRole;

import business.organization.Organization;
import business.useraccount.UserAccount;
import business.workqueue.PoliticianToTeacherWorkRequest;
import business.workqueue.WorkRequest;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rajas
 */
public class TeacherWorkAreaJPanel extends javax.swing.JPanel {
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Organization teacherOrganization;
    /**
     * Creates new form ManageTeacherRoleJPanel
     */
    public TeacherWorkAreaJPanel(JPanel userProcessContainer,Organization organization,UserAccount userAccount) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.teacherOrganization=organization;
        this.userAccount=userAccount;
        populateTable();
    }
    
    public void populateTable() {
        DefaultTableModel dtm = (DefaultTableModel) financeJTable.getModel();
        dtm.setRowCount(0);
        Object[] row = new Object[5];

        for (WorkRequest workRequest : teacherOrganization.getWorkQueue().getWorkQueue()) {
            row[0] = ((PoliticianToTeacherWorkRequest) workRequest).getArea();
            row[1] = workRequest.getSender();
            row[2] = workRequest;
            row[3] = ((PoliticianToTeacherWorkRequest) workRequest).getFunds();
            row[4] = workRequest.getStatus();

            dtm.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        financeJTable = new javax.swing.JTable();
        assignToMeJButton = new javax.swing.JButton();

        financeJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Area Name", "Politician Name", "Message", "Fund Amount", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(financeJTable);

        assignToMeJButton.setText("Assign To Me");
        assignToMeJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignToMeJButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 808, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(assignToMeJButton)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(assignToMeJButton)
                .addContainerGap(294, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void assignToMeJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignToMeJButtonActionPerformed
        // TODO add your handling code here:
        int selectedRow = financeJTable.getSelectedRow();
        
        if (selectedRow >= 0) {
            WorkRequest request = (WorkRequest) financeJTable.getValueAt(selectedRow, 2);
            request.setStatus("Assigned");
            request.setReceiver(userAccount);
            JOptionPane.showMessageDialog(this, "Assigned To You", "Success", JOptionPane.INFORMATION_MESSAGE);
            populateTable();
            
        } else {
            JOptionPane.showMessageDialog(this, "Please Select A Row", "No Input Detected", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_assignToMeJButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignToMeJButton;
    private javax.swing.JTable financeJTable;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
