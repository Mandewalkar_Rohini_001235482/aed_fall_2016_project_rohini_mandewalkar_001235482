/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.politicianrole;

import business.EcoSystem;
import business.enterprise.Enterprise;
import business.organization.FinanceOrganization;
import business.organization.Organization;
import business.useraccount.UserAccount;
import business.workqueue.PoliticianToFinancialOfficerWorkRequest;
import business.workqueue.VolunteerToPoliticianWorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Vineeth Kashyap
 */
public class ViewDetailsJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewDetailsJPanel
     */
    private JPanel userProcessContainer;
    private Organization organization;
    private EcoSystem ecosystem;
    private UserAccount userAccount;
    private VolunteerToPoliticianWorkRequest request;
    private Enterprise enterprise;

    public ViewDetailsJPanel(JPanel userProcessContainer, Organization organization, EcoSystem ecosystem, UserAccount userAccount, VolunteerToPoliticianWorkRequest request, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.ecosystem = ecosystem;
        this.userAccount = userAccount;
        this.request = request;
        this.enterprise = enterprise;
        
        

        populateFields();

    }

    public void populateFields() {
        volunteerJTextField.setText(String.valueOf(request.getSender()));
        areaNameOfVolunteerJTextField.setText(request.getSender().getAreaName());
        noOfIlliterateJTextField.setText(String.valueOf(request.getTotalNoOfIlliteratePeople()));
        totalPopulationJTextField.setText(String.valueOf(request.getTotalNoOfPeople()));
        percentOfIlliterateJTextField.setText(String.valueOf(request.getTotalPercentOfIlliteracy()));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        volunteerJLabel = new javax.swing.JLabel();
        areaNameOfVolunteerJLabel = new javax.swing.JLabel();
        noOfIlliteratePeopleJLabel = new javax.swing.JLabel();
        totalPopulationJLabel = new javax.swing.JLabel();
        percentOfIlliterateJLabel = new javax.swing.JLabel();
        volunteerJTextField = new javax.swing.JTextField();
        areaNameOfVolunteerJTextField = new javax.swing.JTextField();
        noOfIlliterateJTextField = new javax.swing.JTextField();
        totalPopulationJTextField = new javax.swing.JTextField();
        percentOfIlliterateJTextField = new javax.swing.JTextField();
        fundsJLabel = new javax.swing.JLabel();
        submitBtn = new javax.swing.JButton();
        messageToTheFinancialOfficerJLabel = new javax.swing.JLabel();
        messageToTheFinancialOfficerJTextField = new javax.swing.JTextField();
        backBtn = new javax.swing.JButton();
        fundsAllocationJSlider = new javax.swing.JSlider();
        x1000JLabel = new javax.swing.JLabel();

        volunteerJLabel.setText("Volunteer Name");

        areaNameOfVolunteerJLabel.setText("Area Name Of Volunteer");

        noOfIlliteratePeopleJLabel.setText("No Of Illiterate People");

        totalPopulationJLabel.setText("Total Population");

        percentOfIlliterateJLabel.setText("Percent of Illiterate People");

        volunteerJTextField.setEditable(false);

        areaNameOfVolunteerJTextField.setEditable(false);

        noOfIlliterateJTextField.setEditable(false);

        totalPopulationJTextField.setEditable(false);

        percentOfIlliterateJTextField.setEditable(false);

        fundsJLabel.setText("Funds To Allocate");

        submitBtn.setText("Submit");
        submitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitBtnActionPerformed(evt);
            }
        });

        messageToTheFinancialOfficerJLabel.setText("Message to the Financial Officer:");

        backBtn.setText("<< Head Back To Prevous Page");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        fundsAllocationJSlider.setMajorTickSpacing(1);
        fundsAllocationJSlider.setMaximum(10);
        fundsAllocationJSlider.setMinimum(1);
        fundsAllocationJSlider.setMinorTickSpacing(1);
        fundsAllocationJSlider.setPaintLabels(true);
        fundsAllocationJSlider.setPaintTicks(true);
        fundsAllocationJSlider.setSnapToTicks(true);

        x1000JLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        x1000JLabel.setText("(x1000 )");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(messageToTheFinancialOfficerJLabel)
                        .addGap(63, 63, 63)
                        .addComponent(messageToTheFinancialOfficerJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(submitBtn)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(volunteerJLabel)
                            .addComponent(areaNameOfVolunteerJLabel)
                            .addComponent(noOfIlliteratePeopleJLabel)
                            .addComponent(totalPopulationJLabel)
                            .addComponent(percentOfIlliterateJLabel)
                            .addComponent(fundsJLabel))
                        .addGap(100, 100, 100)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(volunteerJTextField)
                            .addComponent(areaNameOfVolunteerJTextField)
                            .addComponent(noOfIlliterateJTextField)
                            .addComponent(totalPopulationJTextField)
                            .addComponent(percentOfIlliterateJTextField)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(x1000JLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(fundsAllocationJSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(backBtn))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backBtn)
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(volunteerJLabel)
                    .addComponent(volunteerJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(areaNameOfVolunteerJLabel)
                    .addComponent(areaNameOfVolunteerJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(noOfIlliteratePeopleJLabel)
                    .addComponent(noOfIlliterateJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totalPopulationJLabel)
                    .addComponent(totalPopulationJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(percentOfIlliterateJLabel)
                    .addComponent(percentOfIlliterateJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fundsJLabel)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(fundsAllocationJSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(x1000JLabel)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(messageToTheFinancialOfficerJLabel)
                    .addComponent(messageToTheFinancialOfficerJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(submitBtn)
                .addContainerGap(157, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void submitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitBtnActionPerformed
        // TODO add your handling code here:
        int funds = fundsAllocationJSlider.getValue();
        String message = messageToTheFinancialOfficerJTextField.getText();
        PoliticianToFinancialOfficerWorkRequest workRequest = new PoliticianToFinancialOfficerWorkRequest(userAccount, null, message, "Funds Requested");
        workRequest.setFunds(funds * 1000);
        workRequest.setAreaName(request.getSender().getAreaName());
        
        Organization org = null;
        for (Organization organization : enterprise.getOrganizationDirectory().getGovernmentOrganizationDirectory()) {
            if (organization instanceof FinanceOrganization) {
                org = organization;
            }
        }
        
        if (org != null) {
            userAccount.getWorkQueue().getWorkQueue().add(workRequest);
            org.getWorkQueue().getWorkQueue().add(workRequest);
            JOptionPane.showMessageDialog(this, "Requested Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_submitBtnActionPerformed

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        // TODO add your handling code here:
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        userProcessContainer.remove(this);
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel areaNameOfVolunteerJLabel;
    private javax.swing.JTextField areaNameOfVolunteerJTextField;
    private javax.swing.JButton backBtn;
    private javax.swing.JSlider fundsAllocationJSlider;
    private javax.swing.JLabel fundsJLabel;
    private javax.swing.JLabel messageToTheFinancialOfficerJLabel;
    private javax.swing.JTextField messageToTheFinancialOfficerJTextField;
    private javax.swing.JTextField noOfIlliterateJTextField;
    private javax.swing.JLabel noOfIlliteratePeopleJLabel;
    private javax.swing.JLabel percentOfIlliterateJLabel;
    private javax.swing.JTextField percentOfIlliterateJTextField;
    private javax.swing.JButton submitBtn;
    private javax.swing.JLabel totalPopulationJLabel;
    private javax.swing.JTextField totalPopulationJTextField;
    private javax.swing.JLabel volunteerJLabel;
    private javax.swing.JTextField volunteerJTextField;
    private javax.swing.JLabel x1000JLabel;
    // End of variables declaration//GEN-END:variables
}
