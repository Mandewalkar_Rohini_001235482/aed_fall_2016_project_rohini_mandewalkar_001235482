/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.role;

import business.EcoSystem;
import business.enterprise.Enterprise;
import business.geography.Network;
import business.organization.Organization;
import business.useraccount.UserAccount;
import javax.swing.JPanel;
import userinterface.volunteerrole.VolunteerWorkAreaJPanel;

/**
 *
 * @author Vineeth Kashyap
 */
public class VolunteerRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise, EcoSystem ecoSystem) {
        return new VolunteerWorkAreaJPanel(userProcessContainer, userAccount, organization, enterprise, ecoSystem);
    }

    @Override
    public String toString() {
        return RoleType.Volunteer.getValue();
    }

}
