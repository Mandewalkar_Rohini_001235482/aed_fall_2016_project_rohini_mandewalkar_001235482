/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.enterprise;

import business.enterprise.Enterprise.EnterpriseType;
import java.util.ArrayList;

/**
 *
 * @author Vineeth Kashyap
 */
public class EnterpriseDirectory {
    private ArrayList<Enterprise> enterpriseDirectory;

    public EnterpriseDirectory() {
        this.enterpriseDirectory = new ArrayList<Enterprise>();
    }

    public ArrayList<Enterprise> getEnterpriseDirectory() {
        return enterpriseDirectory;
    }
    
    public Enterprise createAnEnterprise(String name, EnterpriseType type){
        Enterprise enterprise = null;
        if (type.getValue().equals(EnterpriseType.Government.getValue())) {
            enterprise = new GovernmentEnterprise(name);
            enterpriseDirectory.add(enterprise);
        }
        if (type.getValue().equals(EnterpriseType.NGO.getValue())) {
            enterprise = new NGOEnterprise(name);
            enterpriseDirectory.add(enterprise);
        }
        return enterprise;
    }
    
}
