/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.organization.Organization.GovernmentOrganizationType;
import business.organization.Organization.NGOOrganizationType;
import java.util.ArrayList;

/**
 *
 * @author Vineeth Kashyap
 */
public class OrganizationDirectory {

    private ArrayList<Organization> governmentOrganizationDirectory;
    private ArrayList<VolunteerOrganization> ngoOrganizationDirectory;

    public OrganizationDirectory() {
        this.governmentOrganizationDirectory = new ArrayList<Organization>();
        this.ngoOrganizationDirectory = new ArrayList<VolunteerOrganization>();
    }

    public ArrayList<Organization> getGovernmentOrganizationDirectory() {
        return governmentOrganizationDirectory;
    }

    public ArrayList<VolunteerOrganization> getNgoOrganizationDirectory() {
        return ngoOrganizationDirectory;
    }

    public Organization createAGovernmentOrganization(GovernmentOrganizationType type) {
        Organization organization = null;
        if (type.getValue().equals(GovernmentOrganizationType.Doctor.getValue())) {
            organization = new DoctorOrganization();
            governmentOrganizationDirectory.add(organization);
        }
        if (type.getValue().equals(GovernmentOrganizationType.Politician.getValue())) {
            organization = new PoliticianOrganization();
            governmentOrganizationDirectory.add(organization);
        }
        if (type.getValue().equals(GovernmentOrganizationType.Finance.getValue())) {
            organization = new FinanceOrganization();
            governmentOrganizationDirectory.add(organization);
        }
        if (type.getValue().equals(GovernmentOrganizationType.People.getValue())) {
            organization = new PeopleOrganization();
            governmentOrganizationDirectory.add(organization);
        }
        if (type.getValue().equals(GovernmentOrganizationType.Police.getValue())) {
            organization = new PoliceOrganization();
            governmentOrganizationDirectory.add(organization);
        }
        if (type.getValue().equals(GovernmentOrganizationType.Teacher.getValue())) {
            organization = new TeacherOrganization();
            governmentOrganizationDirectory.add(organization);
        }
        return organization;
    }
    
    public Organization createAnNGOOrganization(NGOOrganizationType type){
        Organization organization = null;
        if (type.getValue().equals(NGOOrganizationType.Volunteer.getValue())) {
            organization = new VolunteerOrganization();
            ngoOrganizationDirectory.add((VolunteerOrganization) organization);
        }
        return organization;
    }

}
