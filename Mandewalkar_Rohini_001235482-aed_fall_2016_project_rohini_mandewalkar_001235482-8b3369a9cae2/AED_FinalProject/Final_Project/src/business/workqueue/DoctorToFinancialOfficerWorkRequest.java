/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue;

import business.geography.City;
import business.useraccount.UserAccount;

/**
 *
 * @author Vineeth Kashyap
 */
public class DoctorToFinancialOfficerWorkRequest extends WorkRequest {
    private int funds;
    private City city;
    
    public DoctorToFinancialOfficerWorkRequest(UserAccount sender, UserAccount receiver, String message, String status) {
        super(sender, receiver, message, status);
    }

    public int getFunds() {
        return funds;
    }

    public void setFunds(int funds) {
        this.funds = funds;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
    
}
