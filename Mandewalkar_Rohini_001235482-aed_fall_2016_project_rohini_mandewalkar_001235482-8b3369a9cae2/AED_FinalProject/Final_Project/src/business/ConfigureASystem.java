/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import business.geography.*;
import business.role.SystemAdminRole;

/**
 *
 * @author Vineeth Kashyap
 */
public class ConfigureASystem {

    public static EcoSystem configure() {
        EcoSystem ecoSystem = EcoSystem.getInstance();
        Network network = ecoSystem.createNetwork("INDIA");
        Network network1 = ecoSystem.createNetwork("USA");
        
        State state1 = network.createANewState("TELANGANA");
        state1.createANewCity("HYDERABAD");
        state1.createANewCity("Karimnagar");
        state1.createANewCity("Warangal");
        
        State state2 = network.createANewState("A.P");
        state2.createANewCity("Vizag");
        state2.createANewCity("Vijayawada");
        state2.createANewCity("Guntur");
        
        State state3 = network.createANewState("Maharashtra");
        state3.createANewCity("Pune");
        state3.createANewCity("Mumbai");
        state3.createANewCity("Shirdi");
        
        State state4 = network1.createANewState("Massachussetts");
        state4.createANewCity("Boston");
        state4.createANewCity("Salem");
        state4.createANewCity("Waltham");
        
        State state5 = network1.createANewState("New York");
        state5.createANewCity("New York City");
        state5.createANewCity("New Jersey");
        state5.createANewCity("Manhattan");
        
        State state6 = network1.createANewState("Texas");
        state6.createANewCity("Arlington");
        state6.createANewCity("Houston");
        state6.createANewCity("Austin");
        
        
        ecoSystem.getUserAccountDirectory().addUserAccount("Admin", "admin", new SystemAdminRole(), ecoSystem.getEmployeeDirectory().createAnEmployee("Vineeth"));
        return ecoSystem;
    }
}
