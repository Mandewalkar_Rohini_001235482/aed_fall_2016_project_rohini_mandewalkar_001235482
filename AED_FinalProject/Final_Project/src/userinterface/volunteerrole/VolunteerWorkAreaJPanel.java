/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.volunteerrole;
import business.EcoSystem;
import business.enterprise.Enterprise;
import business.organization.Organization;
import business.useraccount.UserAccount;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author rajas
 */
public class VolunteerWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form VolunteerWorkAreaJPanel
     */
    private JPanel userProcessContainer;
    private Organization organization;
    private EcoSystem ecosystem;
    private UserAccount userAccount;
    private Enterprise enterprise;

    public VolunteerWorkAreaJPanel(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise, EcoSystem ecosystem) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.ecosystem = ecosystem;
        this.userAccount = userAccount;
        this.enterprise = enterprise;
        areaJLabel.setText(userAccount.getAreaName());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        inputHealthInformationJButton = new javax.swing.JButton();
        inputEducationInformationJButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        areaJLabel = new javax.swing.JLabel();

        inputHealthInformationJButton.setText("Input Health Information");
        inputHealthInformationJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputHealthInformationJButtonActionPerformed(evt);
            }
        });

        inputEducationInformationJButton.setText("Input Education Information");
        inputEducationInformationJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputEducationInformationJButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Area Name");

        areaJLabel.setText("<areaname>");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(areaJLabel))
                    .addComponent(inputEducationInformationJButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(inputHealthInformationJButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(504, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(areaJLabel)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addComponent(inputHealthInformationJButton)
                .addGap(18, 18, 18)
                .addComponent(inputEducationInformationJButton)
                .addContainerGap(296, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void inputHealthInformationJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputHealthInformationJButtonActionPerformed
        // TODO add your handling code here:
        VolunteerHealthJPanel health = new VolunteerHealthJPanel(userProcessContainer, organization, ecosystem, userAccount);
        userProcessContainer.add("VolunteerHealthJPanel", health);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_inputHealthInformationJButtonActionPerformed

    private void inputEducationInformationJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputEducationInformationJButtonActionPerformed
        // TODO add your handling code here:
        VolunteerEducationJPanel education = new VolunteerEducationJPanel(userProcessContainer, organization, ecosystem, userAccount, enterprise);
        userProcessContainer.add("ManageEducationWorkAreaJPanel", education);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_inputEducationInformationJButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel areaJLabel;
    private javax.swing.JButton inputEducationInformationJButton;
    private javax.swing.JButton inputHealthInformationJButton;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
