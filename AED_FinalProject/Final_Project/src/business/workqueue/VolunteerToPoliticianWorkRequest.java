/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue;

import business.useraccount.UserAccount;

/**
 *
 * @author rajas
 */
public class VolunteerToPoliticianWorkRequest extends WorkRequest {

    private int Noofilliteratechildrenbelow15;
    private int Noofilliteratestudentsaboveage15;
    private int IlliterateMales;
    private int IlliterateFemales;
    private int TotalNoOfIlliteratePeople;
    private int totalNoOfPeople;
    private double TotalPercentOfIlliteracy;
    private String year;
    private int funds;

    public VolunteerToPoliticianWorkRequest(UserAccount sender, UserAccount receiver, String message, String status) {
        super(sender, receiver, message, status);
    }

    public int getNoofilliteratechildrenbelow15() {
        return Noofilliteratechildrenbelow15;
    }

    public void setNoofilliteratechildrenbelow15(int Noofilliteratechildrenbelow15) {
        this.Noofilliteratechildrenbelow15 = Noofilliteratechildrenbelow15;
    }

    public int getNoofilliteratestudentsaboveage15() {
        return Noofilliteratestudentsaboveage15;
    }

    public void setNoofilliteratestudentsaboveage15(int Noofilliteratestudentsaboveage15) {
        this.Noofilliteratestudentsaboveage15 = Noofilliteratestudentsaboveage15;
    }

    public int getIlliterateMales() {
        return IlliterateMales;
    }

    public void setIlliterateMales(int IlliterateMales) {
        this.IlliterateMales = IlliterateMales;
    }

    public int getIlliterateFemales() {
        return IlliterateFemales;
    }

    public void setIlliterateFemales(int IlliterateFemales) {
        this.IlliterateFemales = IlliterateFemales;
    }

    public int getTotalNoOfIlliteratePeople() {
        return TotalNoOfIlliteratePeople;
    }

    public void setTotalNoOfIlliteratePeople(int TotalNoOfIlliteratePeople) {
        this.TotalNoOfIlliteratePeople = TotalNoOfIlliteratePeople;
    }

    public double getTotalPercentOfIlliteracy() {
        return TotalPercentOfIlliteracy;
    }

    public void setTotalPercentOfIlliteracy(double TotalPercentOfIlliteracy) {
        this.TotalPercentOfIlliteracy = TotalPercentOfIlliteracy;
    }

    public int getTotalNoOfPeople() {
        return totalNoOfPeople;
    }

    public void setTotalNoOfPeople(int totalNoOfPeople) {
        this.totalNoOfPeople = totalNoOfPeople;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getFunds() {
        return funds;
    }

    public void setFunds(int funds) {
        this.funds = funds;
    }
}
