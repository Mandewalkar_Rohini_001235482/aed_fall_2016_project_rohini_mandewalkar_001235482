/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue;

import business.useraccount.UserAccount;

/**
 *
 * @author rajas
 */
public class PoliticianToFinancialOfficerWorkRequest extends WorkRequest {

    private int funds;
    private String currencyUnits;
    private String areaName;

    public PoliticianToFinancialOfficerWorkRequest(UserAccount sender, UserAccount receiver, String message, String status) {
        super(sender, receiver, message, status);
    }

    public int getFunds() {
        return funds;
    }

    public void setFunds(int funds) {
        this.funds = funds;
    }

    public String getCurrencyUnits() {
        return currencyUnits;
    }

    public void setCurrencyUnits(String currencyUnits) {
        this.currencyUnits = currencyUnits;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

}
