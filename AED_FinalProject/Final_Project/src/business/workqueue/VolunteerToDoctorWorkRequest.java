/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.workqueue;

import business.geography.City;
import business.geography.Network;
import business.geography.State;
import business.useraccount.UserAccount;

/**
 *
 * @author Vineeth Kashyap
 */
public class VolunteerToDoctorWorkRequest extends WorkRequest{
    private int numberOfMalnourishedPeople;
    private double percentageOfMalnourishedPeople;
    private int numberOfPeopleBelowPovertyLine;
    private double percentageOfPeopleBelowPovertyLine;
    private int numberOfPeopleAffectedFromNaturalDisasters;
    private double percentageOfPeopleAffectedFromNaturalDisasters;
    private int totalNumberOfPeople;
    private Network network;
    private State state;
    private City city;
    
    public VolunteerToDoctorWorkRequest(UserAccount sender, UserAccount receiver, String message, String status) {
        super(sender, receiver, message, status);        
    }

    public int getNumberOfMalnourishedPeople() {
        return numberOfMalnourishedPeople;
    }

    public void setNumberOfMalnourishedPeople(int numberOfMalnourishedPeople) {
        this.numberOfMalnourishedPeople = numberOfMalnourishedPeople;
    }

    public int getNumberOfPeopleBelowPovertyLine() {
        return numberOfPeopleBelowPovertyLine;
    }

    public void setNumberOfPeopleBelowPovertyLine(int numberOfPeopleBelowPovertyLine) {
        this.numberOfPeopleBelowPovertyLine = numberOfPeopleBelowPovertyLine;
    }

    public int getNumberOfPeopleAffectedFromNaturalDisasters() {
        return numberOfPeopleAffectedFromNaturalDisasters;
    }

    public void setNumberOfPeopleAffectedFromNaturalDisasters(int numberOfPeopleAffectedFromNaturalDisasters) {
        this.numberOfPeopleAffectedFromNaturalDisasters = numberOfPeopleAffectedFromNaturalDisasters;
    }

    public int getTotalNumberOfPeople() {
        return totalNumberOfPeople;
    }

    public void setTotalNumberOfPeople(int totalNumberOfPeople) {
        this.totalNumberOfPeople = totalNumberOfPeople;
    }

    public Network getNetwork() {
        return network;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public double getPercentageOfMalnourishedPeople() {
        percentageOfMalnourishedPeople = numberOfMalnourishedPeople*100/totalNumberOfPeople;
        return percentageOfMalnourishedPeople;
    }

    public double getPercentageOfPeopleBelowPovertyLine() {
        percentageOfPeopleBelowPovertyLine = numberOfPeopleBelowPovertyLine*100/totalNumberOfPeople;
        return percentageOfPeopleBelowPovertyLine;
    }

    public double getPercentageOfPeopleAffectedFromNaturalDisasters() {
        percentageOfPeopleAffectedFromNaturalDisasters = numberOfPeopleAffectedFromNaturalDisasters*100/totalNumberOfPeople;
        return percentageOfPeopleAffectedFromNaturalDisasters;
    }
    
}
