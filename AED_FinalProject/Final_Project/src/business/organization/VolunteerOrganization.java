/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.role.Role;
import business.role.VolunteerRole;
import java.util.ArrayList;

/**
 *
 * @author Vineeth Kashyap
 */
public class VolunteerOrganization extends Organization{

    public VolunteerOrganization() {
        super(NGOOrganizationType.Volunteer.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> supportedRoles = new ArrayList<Role>();
        supportedRoles.add(new VolunteerRole());
        return supportedRoles;
    }
    
}
